#ifndef CONFIG_H
#define CONFIG_H

/******** 主窗口配置数据 ********/
#define GAME_WIDTH 512 //宽度
#define GAME_HEIGHT 768 //高度
#define GAME_TITLE "飞机大战 v1.0" //标题
#define GAME_RATE 10 //定时器间隔，单位：ms
#define ICON_PATH ":/res/icon.png" //窗口图标

/******** 地图配置数据 ********/
#define MAP_PATH ":/res/map.jpg" //地图路径
#define MAP_SCROLL_SPEED 2 //滚动速度

/******** 主机配置数据 ********/
#define HEROPLANE_PATH ":/res/hero.png" //主机路径
#define HERO_LIFE 10 //主机生命值
#define HERO_SKILL 20 //主机技能值

/******** 杂鱼配置数据 ********/
#define FISHENEMY_PATH ":/res/diji.png" //杂鱼路径
#define FISH_SPEED 3 //杂鱼速度
#define FISH_SHOW_INTERVAL 150 //杂鱼出场时间间隔
#define FISH_NUM 50 //杂鱼数量

class config
{
public:
    config();
};

#endif // CONFIG_H
