#include "heroplane.h"
#include "config.h"

HeroPlane::HeroPlane()
{
    //初始化加载主机图片对象
    heroPlane.load(HEROPLANE_PATH);

    //初始化主机坐标
    x = (GAME_WIDTH - heroPlane.width()) * 0.5;
    y = GAME_HEIGHT - heroPlane.height();

    //初始化矩形边框
    myRect.setWidth(heroPlane.width());
    myRect.setHeight(heroPlane.height());
    myRect.moveTo(x,y);

    //设置生命值
    life = HERO_LIFE;

    //设置技能值
    skill = HERO_SKILL;

    //初始化射击时间记录
    recorder = 0;
}

//位置函数
void HeroPlane::setPosition(int x, int y){
    this->x = x;
    this->y = y;
    myRect.moveTo(x,y);
}
