#ifndef MAP_H
#define MAP_H
#include <QPixmap>

class Map
{
public:
    Map();

    //地图滚动坐标计算
    void mapPosition();

    //地图图片
    QPixmap map1;
    QPixmap map2;

    //地图Y轴坐标
    int map1_Y;
    int map2_Y;

    //地图滚动速度
    int scroll_speed;
};

#endif // MAP_H
