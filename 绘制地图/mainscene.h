#ifndef MAINSCENE_H
#define MAINSCENE_H


#include <QWidget>
#include "map.h"


class MainScene : public QWidget
{
    Q_OBJECT

public:
    MainScene(QWidget *parent = nullptr);
    ~MainScene();

    //初始化窗口大小
    void initScene();

    //启动游戏
    void playGame();

    //更新游戏中所有元素坐标
    void updatePosition();

    //绘制屏幕事件
    void paintEvent(QPaintEvent *event);

    //地图对象
    Map map;

    //定时器对象
    QTimer timer;
};
#endif // MAINSCENE_H
