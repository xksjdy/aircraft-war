#ifndef CONFIG_H
#define CONFIG_H

/******** 主窗口配置数据 ********/
#define GAME_WIDTH 512 //宽度
#define GAME_HEIGHT 768 //高度
#define GAME_TITLE "飞机大战 v1.0" //标题
#define GAME_RATE 10 //定时器间隔，单位：ms
#define ICON_PATH ":/res/icon.png" //窗口图标


/******** 地图配置数据 ********/
#define MAP_PATH ":/res/map.jpg" //地图路径
#define MAP_SCROLL_SPEED 2 //滚动速度

class config
{
public:
    config();
};

#endif // CONFIG_H
