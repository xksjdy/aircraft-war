#include "bomb.h"
#include "config.h"

Bomb::Bomb()
{
    //初始化将所有爆炸图片放入数组中
    for(int i = 1;i <= BOMB_MAX;i++){
        QString str = QString(BOMB_PATH).arg(i);
        bombPix.push_back(QPixmap(str));
    }

    //设置爆炸时间记录
    recorder = 0;

    //设置爆炸位置
    x = 0;
    y = 0;

    //设置图片下标
    index = 0;

    //设置爆炸状态（未播放过）
    isPlayed = false;
}

void Bomb::updateInfo(){

    //累加爆炸时间记录
    recorder++;

    //若未到爆炸间隔，则直接返回
    if(recorder < BOMB_INTERVAL){
        return;
    }

    //否则时间记录置零
    recorder = 0;

    //切到下一张爆炸图片
    index++;

    //若全部图片都已经播放过，则改变播放状态
    if(index >= BOMB_MAX){
        isPlayed = true;
        index = 0;
    }
}
