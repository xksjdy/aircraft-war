#include "boss.h"
#include "config.h"

Boss::Boss()
{
    //初始化加载图片对象
    boss.load(BOSS_PATH);

    //设置坐标
    x = 0;
    y = 0;

    //初始化矩形边框
    bossRect.setWidth(boss.width());
    bossRect.setHeight(boss.height());
    bossRect.moveTo(x,y);

    //不在界面内（空闲）
    isFree = true;

    //尚未被摧毁
    isDestoryed = false;

    //设置速度
    speed = BOSS_SPEED;

    //设置生命值
    life = BOSS_LIFE;

    //初始化射击时间记录
    recorder = 0;
}

//位置更新函数
void Boss::updatePosition(){
    //若不在界面内（空闲），则直接返回
    if(isFree){
        return;
    }

    //否则向下运动
    y += BOSS_SPEED;
    bossRect.moveTo(x,y);

    //超出界面则重新置为空闲
    if(y >GAME_HEIGHT + bossRect.height()){
        isFree = true;
    }
}

//射击函数，注意有三个弹匣，所以需要有三个循环
void Boss::shoot(){
    //累加时间记录
    recorder++;

    //若当前记录时间小于射击间隔，则返回
    if(recorder < BOSS_INTERVAL){
        return;
    }

    //若达到射击间隔，则将时间记录重新置零
    recorder = 0;

    //发射子弹
    for(int i = 0;i < BULLET_NUM;i++){
        //选择空闲子弹进行发射
        if(bossBullet1[i].isFree){
            //设置发射位置
            bossBullet1[i].boss_x = x + bossRect.width() * 0.5 - 75;
            bossBullet1[i].boss_y = y + 25;
            //发射后更改子弹空闲状态
            bossBullet1[i].isFree = false;
            break;
        }
    }

    for(int i = 0;i < BULLET_NUM;i++){
        //选择空闲子弹进行发射
        if(bossBullet2[i].isFree){
            //设置发射位置
            bossBullet2[i].boss_x = x + bossRect.width() * 0.5 - 15;
            bossBullet2[i].boss_y = y + 25;
            //发射后更改子弹空闲状态
            bossBullet2[i].isFree = false;
            break;
        }
    }

    for(int i = 0;i < BULLET_NUM;i++){
        //选择空闲子弹进行发射
        if(bossBullet3[i].isFree){
            //设置发射位置
            bossBullet3[i].boss_x = x + bossRect.width() * 0.5 + 45;
            bossBullet3[i].boss_y = y + 25;
            //发射后更改子弹空闲状态
            bossBullet3[i].isFree = false;
            break;
        }
    }
}




