#ifndef FISHENEMY_H
#define FISHENEMY_H

#include <QPixmap>
#include <QRect>
#include "bullet.h"
#include "config.h"

class FishEnemy
{
public:
    //构造函数
    FishEnemy();

    //杂鱼坐标
    int x;
    int y;

    //杂鱼速度
    int speed;

    //飞机是否在界面内（空闲表示不在）
    bool isFree;

    //是否被摧毁
    bool isDestoryed;

    //杂鱼图片对象
    QPixmap fishEnemy;

    //矩形边框（碰撞检测）
    QRect fishRect;

    //弹匣
    Bullet fishBullet[BULLET_NUM];

    //射击时间记录
    int recorder;

    //射击函数
    void shoot();

    //位置更新函数
    void updatePosition();
};

#endif // FISHENEMY_H
