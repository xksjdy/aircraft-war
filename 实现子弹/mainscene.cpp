#include "mainscene.h"
#include "config.h"
#include <QIcon>
#include <QTimer>
#include <QPainter>
#include <QMouseEvent>

MainScene::MainScene(QWidget *parent)
    : QWidget(parent)
{
    //初始化场景
    initScene();

    //启动游戏
    playGame();
}

void MainScene::initScene(){
    //初始化窗口大小
    setFixedSize(GAME_WIDTH,GAME_HEIGHT);

    //设置窗口标题
    setWindowTitle(GAME_TITLE);

    //设置窗口图标
    setWindowIcon(QIcon(ICON_PATH));

    //设置定时器
    timer.setInterval(GAME_RATE);

    //设置敌机出场间隔
    fishRecorder = 0;
    bossRecorder = 0;

    //设置随机数种子
    srand((unsigned int)time(nullptr));
}

void MainScene::playGame(){
    //启动计时器
    timer.start();

    //监听计时器
    connect(&timer,&QTimer::timeout,[=](){
        //更新游戏中元素坐标
        updatePosition();

        //敌机出场
        showFish();
        showBoss();

        //刷新屏幕
        update();
    });
}

//更新游戏中所有元素坐标
void MainScene::updatePosition(){
    //更新地图坐标
    map.mapPosition();

    //发射子弹
    hero.shoot();

    //更新非空闲子弹位置
    for(int i = 0;i < BULLET_NUM;i++){
        //若非空闲则计算位置
        if(!hero.myBullet[i].isFree){
            hero.myBullet[i].updatePosition();
        }
    }

    //杂鱼元素更新
    for(int i = 0;i < FISH_NUM;i++){
        //杂鱼射击和位置更新
        if(!fish[i].isDestoryed && !fish[i].isFree){
            fish[i].shoot();
            fish[i].updatePosition();
        }

        //杂鱼子弹位置更新
        for(int j = 0;j < BULLET_NUM;j++){
            fish[i].fishBullet[j].updateEnemyPosition();
        }
    }

    //Boss元素更新
    for(int i = 0;i < BOSS_NUM;i++){
        //boss射击和位置更新
        if(!boss[i].isDestoryed && !boss[i].isFree){
            boss[i].shoot();
            boss[i].updatePosition();
        }

        //boss子弹位置更新
        for(int j = 0;j < BULLET_NUM;j++){
            boss[i].bossBullet1[j].updateBossPositionLeft();
            boss[i].bossBullet2[j].updateBossPosition();
            boss[i].bossBullet3[j].updateBossPositionRight();
        }
    }

}

//绘制屏幕事件
void MainScene::paintEvent(QPaintEvent *){
    QPainter painter(this);

    //绘制地图
    painter.drawPixmap(0,map.map1_Y,map.map1);
    painter.drawPixmap(0,map.map2_Y,map.map2);

    //绘制主机
    painter.drawPixmap(hero.x,hero.y,hero.heroPlane);

    //绘制子弹
    for(int i = 0;i < BULLET_NUM;i++){
        //若非空闲则绘制
        if(!hero.myBullet[i].isFree){
            painter.drawPixmap(hero.myBullet[i].my_x,hero.myBullet[i].my_y,hero.myBullet[i].myBullet);
        }
    }

    //绘制杂鱼
    for(int i = 0;i < FISH_NUM;i++){
        //若非空闲则绘制
        if(!fish[i].isFree){
           //若非空闲且未被摧毁则绘制杂鱼
            if(!fish[i].isDestoryed){
                painter.drawPixmap(fish[i].x,fish[i].y,fish[i].fishEnemy);
            }
        }

        //绘制杂鱼子弹
        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!fish[i].fishBullet[j].isFree){
                painter.drawPixmap(fish[i].fishBullet[j].enemy_x,fish[i].fishBullet[j].enemy_y,fish[i].fishBullet[j].enemyBullet);
            }
        }
    }

    //绘制boss
    for(int i = 0;i < BOSS_NUM;i++){
        //若非空闲则绘制
        if(!boss[i].isFree){
           //若非空闲且未被摧毁则绘制boss
            if(!boss[i].isDestoryed){
                painter.drawPixmap(boss[i].x,boss[i].y,boss[i].boss);
            }                 
        }

        //绘制boss子弹
        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!boss[i].bossBullet1[j].isFree){
                painter.drawPixmap(boss[i].bossBullet1[j].boss_x,boss[i].bossBullet1[j].boss_y,boss[i].bossBullet1[j].bossBullet);
            }
        }

        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!boss[i].bossBullet2[j].isFree){
                painter.drawPixmap(boss[i].bossBullet2[j].boss_x,boss[i].bossBullet2[j].boss_y,boss[i].bossBullet2[j].bossBullet);
            }
        }

        for(int j = 0;j < BULLET_NUM;j++){
             //若非空闲则绘制
             if(!boss[i].bossBullet3[j].isFree){
               painter.drawPixmap(boss[i].bossBullet3[j].boss_x,boss[i].bossBullet3[j].boss_y,boss[i].bossBullet3[j].bossBullet);
             }
        }
    }
}

//杂鱼出场
void MainScene::showFish(){
    //累加时间记录
    fishRecorder++;

    //若未到达出场间隔则直接返回
    if(fishRecorder < FISH_SHOW_INTERVAL){
        return;
    }

    //否则重新置零
    fishRecorder = 0;

    //设置出场位置
    for(int i = 0;i < FISH_NUM;i++){
        //空闲且未被摧毁则出场
        if(fish[i].isFree && !fish[i].isDestoryed){
            fish[i].isFree = false;
            fish[i].x = rand()%(GAME_WIDTH - fish[i].fishRect.width());
            fish[i].y = -fish[i].fishRect.height();
            break;
        }
    }
}

//boss出场
void MainScene::showBoss(){
    //累加时间记录
    bossRecorder++;

    //若未到达出场间隔则直接返回
    if(bossRecorder < BOSS_SHOW_INTERVAL){
        return;
    }

    //否则重新置零
    bossRecorder = 0;

    //设置出场位置
    for(int i = 0;i < BOSS_NUM;i++){
        //空闲且未被摧毁则出场
        if(boss[i].isFree && !boss[i].isDestoryed){
            boss[i].isFree = false;
            boss[i].x = (GAME_WIDTH - boss[i].bossRect.width()) * 0.5;
            boss[i].y = -boss[i].bossRect.height();
            break;
        }
    }
}

//移动鼠标事件
void MainScene::mouseMoveEvent(QMouseEvent *event){
    int x = event->x() - hero.myRect.width() * 0.5;
    int y = event->y() - hero.myRect.height() * 0.5;

    //改变飞机坐标，注意边界检测
    if(x <= 0){
        x = 0;
    }
    if(x >= GAME_WIDTH- hero.myRect.width()){
        x = GAME_WIDTH- hero.myRect.width();
    }
    if(y <= 0){
        y = 0;
    }
    if(y >= GAME_HEIGHT- hero.myRect.height()){
        y = GAME_HEIGHT- hero.myRect.height();
    }

    hero.setPosition(x,y);
}

MainScene::~MainScene()
{
}

