#ifndef BOSS_H
#define BOSS_H

#include <QPixmap>
#include <QRect>
#include "config.h"
#include "bullet.h"

class Boss
{
public:
    //构造函数
    Boss();

    //boss坐标
    int x;
    int y;

    //boss速度
    int speed;

    //是否被摧毁
    bool isDestoryed;

    //是否在界面内（空闲表示不在）
    bool isFree;

    //生命值
    int life;

    //boos图片对象
    QPixmap boss;

    //矩形边框（碰撞检测）
    QRect bossRect;

    //射击时间记录
    int recorder;

    //弹匣
    Bullet bossBullet1[BULLET_NUM];
    Bullet bossBullet2[BULLET_NUM];
    Bullet bossBullet3[BULLET_NUM];

    //射击函数
    void shoot();

    //位置更新函数
    void updatePosition();
};

#endif // BOSS_H
