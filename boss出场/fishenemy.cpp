#include "fishenemy.h"
#include "config.h"

FishEnemy::FishEnemy()
{
    //初始化加载杂鱼图片对象
    fishEnemy.load(FISHENEMY_PATH);

    //设置杂鱼坐标
    x = 0;
    y = 0;

    //初始化矩形边框
    fishRect.setWidth(fishEnemy.width());
    fishRect.setHeight(fishEnemy.height());
    fishRect.moveTo(x,y);

    //不在界面内（空闲）
    isFree = true;

    //尚未被摧毁
    isDestoryed = false;

    //设置速度
    speed = FISH_SPEED;
}

//位置更新函数
void FishEnemy::updatePosition(){
    //若不在界面内（空闲），则直接返回
    if(isFree){
        return;
    }

    //否则向下运动
    y += FISH_SPEED;
    fishRect.moveTo(x,y);

    //超出界面则重新置为空闲
    if(y >GAME_HEIGHT + fishRect.height()){
        isFree = true;
    }
}

