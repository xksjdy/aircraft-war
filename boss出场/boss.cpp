#include "boss.h"
#include "config.h"

Boss::Boss()
{
    //初始化加载图片对象
    boss.load(BOSS_PATH);

    //设置坐标
    x = 0;
    y = 0;

    //初始化矩形边框
    bossRect.setWidth(boss.width());
    bossRect.setHeight(boss.height());
    bossRect.moveTo(x,y);

    //不在界面内（空闲）
    isFree = true;

    //尚未被摧毁
    isDestoryed = false;

    //设置速度
    speed = BOSS_SPEED;

    //设置生命值
    life = BOSS_LIFE;   
}

//位置更新函数
void Boss::updatePosition(){
    //若不在界面内（空闲），则直接返回
    if(isFree){
        return;
    }

    //否则向下运动
    y += BOSS_SPEED;
    bossRect.moveTo(x,y);

    //超出界面则重新置为空闲
    if(y >GAME_HEIGHT + bossRect.height()){
        isFree = true;
    }
}






