#ifndef MAINSCENE_H
#define MAINSCENE_H


#include <QWidget>
#include <QTimer>
#include "map.h"
#include "heroplane.h"
#include "fishenemy.h"
#include "boss.h"

class MainScene : public QWidget
{
    Q_OBJECT

public:
    MainScene(QWidget *parent = nullptr);
    ~MainScene();

    //杂鱼出场间隔
    int fishRecorder;

    //boss出场间隔
    int bossRecorder;

    //初始化窗口大小
    void initScene();

    //启动游戏
    void playGame();

    //更新游戏中所有元素坐标
    void updatePosition();

    //绘制屏幕事件
    void paintEvent(QPaintEvent *event);

    //移动鼠标事件
    void mouseMoveEvent(QMouseEvent *event);

    //Boss出场
    void showBoss();

    //杂鱼出场
    void showFish();

    //地图对象
    Map map;

    //主机对象
    HeroPlane hero;

    //杂鱼对象数组
    FishEnemy fish[FISH_NUM];

    //Boss对象数组
    Boss boss[BOSS_NUM];

    //定时器对象
    QTimer timer;
};
#endif // MAINSCENE_H
