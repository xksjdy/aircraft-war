#ifndef BOMB_H
#define BOMB_H

#include <QPixmap>
#include <QVector>
#include <QString>

class Bomb
{
public:
    //构造函数
    Bomb();

    //爆炸图片数组
    QVector<QPixmap> bombPix;

    //图片下标
    int index;

    //爆炸时间记录
    int recorder;

    //爆炸图片更新函数
    void updateInfo();

    //爆炸状态（爆炸图片是否播放过）
    bool isPlayed;

    //爆炸位置
    int x;
    int y;
};

#endif // BOMB_H
