#ifndef CONFIG_H
#define CONFIG_H

/******** 主窗口配置数据 ********/
#define GAME_WIDTH 512 //宽度
#define GAME_HEIGHT 768 //高度
#define GAME_TITLE "飞机大战 v2.5" //标题
#define GAME_RATE 10 //定时器间隔，单位：ms
#define ICON_PATH ":/res/icon.png" //窗口图标
#define LIFE "生命值：" //生命值显示
#define SCORE "得分：" //得分显示
#define SKILL "技能值" //技能值显示

/******** 地图配置数据 ********/
#define MAP_PATH ":/res/map.jpg" //地图路径
#define MAP_SCROLL_SPEED 2 //滚动速度

/******** 子弹配置数据 ********/
#define MY_BULLET_PATH ":/res/zidan1.png" //我方子弹路径
#define ENEMY_BULLET_PATH ":/res/enemyBullet.png" //敌方子弹路径
#define BOSS_BULLET_PATH ":/res/bossBullet.png" //boss子弹路径
#define X_BULLET_SPEED 2 //子弹x方向速度
#define Y_BULLET_SPEED 4 //子弹y方向速度
#define BULLET_NUM 30 //弹匣中子弹数量
#define LIFE_SUPPLY ":/res/skill2.png" //回血包路径
#define LIFE_SUPPLY_NUM 10 //回血包数量
#define SUPPLY_SHOW_INTERVAL 200 //回血包掉落间隔
#define SUPPLY_SPEED 4 //回血包掉落速度

/******** 主机配置数据 ********/
#define HEROPLANE_PATH ":/res/hero.png" //主机路径
#define INTERVAL 30 //射击时间间隔
#define HERO_LIFE 10 //主机生命值
#define HERO_SKILL 20 //主机技能值
#define BIG_BULLET_NUM 5 //主机导弹数
#define BIG_BULLET_PATH ":/res/skill1.png" //导弹路径

/******** 杂鱼配置数据 ********/
#define FISHENEMY_PATH ":/res/diji.png" //杂鱼路径1
#define FISHENEMY2_PATH ":/res/diji2.png" //杂鱼路径2
#define FISH_SPEED 3 //杂鱼速度
#define FISH_INTERVAL 30 //射击间隔
#define FISH_SHOW_INTERVAL 100 //杂鱼出场时间间隔
#define FISH_NUM 50 //杂鱼数量

/******** boss配置数据 ********/
#define BOSS_PATH ":/res/boss.png" //boss路径
#define BOSS_SPEED 1 //boss速度
#define BOSS_INTERVAL 30 //射击间隔
#define BOSS_SHOW_INTERVAL 2000 //boss出场时间间隔
#define BOSS_LIFE 20 //boss生命值
#define BOSS_NUM 2 //boss数量
#define BOSS_BOMB_NUM 5 //boss爆炸图片数

/******** 爆炸效果配置数据 ********/
#define BOMB_PATH ":/res/bomb-%1.png" //爆炸资源图片
#define BOMB_NUM 20 //爆炸数量
#define BOMB_MAX 4 //爆炸图片最大索引
#define BOMB_INTERVAL 20 //爆炸切图间隔

class config
{
public:
    config();
};

#endif // CONFIG_H
