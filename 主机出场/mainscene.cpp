#include "mainscene.h"
#include "config.h"
#include <QIcon>
#include <QTimer>
#include <QPainter>
#include <QMouseEvent>

MainScene::MainScene(QWidget *parent)
    : QWidget(parent)
{
    //初始化场景
    initScene();

    //启动游戏
    playGame();
}

void MainScene::initScene(){
    //初始化窗口大小
    setFixedSize(GAME_WIDTH,GAME_HEIGHT);

    //设置窗口标题
    setWindowTitle(GAME_TITLE);

    //设置窗口图标
    setWindowIcon(QIcon(ICON_PATH));

    //设置定时器
    timer.setInterval(GAME_RATE);
}

void MainScene::playGame(){
    //启动计时器
    timer.start();

    //监听计时器
    connect(&timer,&QTimer::timeout,[=](){
        //更新游戏中元素坐标
        updatePosition();

        //刷新屏幕
        update();
    });
}

//更新游戏中所有元素坐标
void MainScene::updatePosition(){
    //更新地图坐标
    map.mapPosition();
}

//绘制屏幕事件
void MainScene::paintEvent(QPaintEvent *){
    QPainter painter(this);

    //绘制地图
    painter.drawPixmap(0,map.map1_Y,map.map1);
    painter.drawPixmap(0,map.map2_Y,map.map2);

    //绘制主机
    painter.drawPixmap(hero.x,hero.y,hero.heroPlane);

}

//移动鼠标事件
void MainScene::mouseMoveEvent(QMouseEvent *event){
    int x = event->x() - hero.myRect.width() * 0.5;
    int y = event->y() - hero.myRect.height() * 0.5;

    //改变飞机坐标，注意边界检测
    if(x <= 0){
        x = 0;
    }
    if(x >= GAME_WIDTH- hero.myRect.width()){
        x = GAME_WIDTH- hero.myRect.width();
    }
    if(y <= 0){
        y = 0;
    }
    if(y >= GAME_HEIGHT- hero.myRect.height()){
        y = GAME_HEIGHT- hero.myRect.height();
    }

    hero.setPosition(x,y);
}

MainScene::~MainScene()
{
}

