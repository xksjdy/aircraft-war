#ifndef GAMEOVER_H
#define GAMEOVER_H

#include <QWidget>
#include <QPaintEvent>

namespace Ui {
class GameOver;
}

class GameOver : public QWidget
{
    Q_OBJECT

public:
    explicit GameOver(QWidget *parent = nullptr);
    ~GameOver();
    int score;
    void paintEvent(QPaintEvent *event);

private:
    Ui::GameOver *ui;
};

#endif // GAMEOVER_H
