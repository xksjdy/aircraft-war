#include "mainscene.h"
#include "config.h"
#include <QIcon>
#include <QTimer>
#include <QPainter>
#include <QMouseEvent>

MainScene::MainScene(QWidget *parent)
    : QWidget(parent)
{
    //初始化场景
    initScene();

    //启动游戏
    playGame();

    //初始化得分
    score = 0;

    ui = new GameOver();
}

void MainScene::initScene(){
    //初始化窗口大小
    setFixedSize(GAME_WIDTH,GAME_HEIGHT);

    //设置窗口标题
    setWindowTitle(GAME_TITLE);

    //设置窗口图标
    setWindowIcon(QIcon(ICON_PATH));

    //设置定时器
    timer.setInterval(GAME_RATE);

    //设置敌机、回血包出场间隔
    fishRecorder = 0;
    supplyRecorder = 0;

    //设置随机数种子
    srand((unsigned int)time(nullptr));

    //设置得分显示和生命值显示
    Life = LIFE;
    Score = SCORE;
    Skill = SKILL;
}

void MainScene::playGame(){
    //启动计时器
    timer.start();

    //监听计时器
    connect(&timer,&QTimer::timeout,[=](){
        //更新游戏中元素坐标
        updatePosition();

        //敌机出场
        showFish();
        showBoss();
        showSupply();

        //游戏结束事件
        game_over();

        //碰撞检测
        collisionDetection();

        //刷新屏幕
        update();
    });
}

//更新游戏中所有元素坐标
void MainScene::updatePosition(){
    //更新地图坐标
    map.mapPosition();

    //发射子弹
    hero.shoot();

    //更新非空闲子弹位置
    for(int i = 0;i < BULLET_NUM;i++){
        //若非空闲则计算位置
        if(!hero.myBullet[i].isFree){
            hero.myBullet[i].updatePosition();
        }
    }

    //更新非空闲导弹位置
    for(int i = 0;i < BIG_BULLET_NUM;i++){
        //若非空闲则计算位置
        if(!hero.bigBullet[i].isFree){
            hero.bigBullet[i].updateBigPosition();
        }
    }

    //杂鱼元素更新
    for(int i = 0;i < FISH_NUM;i++){
        //杂鱼射击和位置更新
        if(!fish[i].isDestoryed && !fish[i].isFree){
            fish[i].shoot();
            fish[i].updatePosition();
        }

        //杂鱼爆炸更新
        if(fish[i].isDestoryed && !fish[i].isFree){
            fish[i].fishBomb.updateInfo();
        }

        //杂鱼子弹位置更新
        for(int j = 0;j < BULLET_NUM;j++){
            fish[i].fishBullet[j].updateEnemyPosition();
        }
    }

    //Boss元素更新
    for(int i = 0;i < BOSS_NUM;i++){
        //boss射击和位置更新
        if(!boss[i].isDestoryed && !boss[i].isFree){
            boss[i].shoot();
            boss[i].updatePosition();
        }

        //boss爆炸更新
        if(boss[i].isDestoryed && !boss[i].isFree){
            for(int j = 0;j < BOSS_BOMB_NUM;j++){
                boss[i].bossBomb[j].updateInfo();
            }
        }

        //boss子弹位置更新
        for(int j = 0;j < BULLET_NUM;j++){
            boss[i].bossBullet1[j].updateBossPositionLeft();
            boss[i].bossBullet2[j].updateBossPosition();
            boss[i].bossBullet3[j].updateBossPositionRight();
        }
    }

    //回血包元素更新
    for(int i = 0;i < LIFE_SUPPLY_NUM;i++){
        lifeSupply[i].updateSupplyPosition();
    }

}

//绘制屏幕事件
void MainScene::paintEvent(QPaintEvent *){
    QPainter painter(this);

    //绘制地图
    painter.drawPixmap(0,map.map1_Y,map.map1);
    painter.drawPixmap(0,map.map2_Y,map.map2);

    //绘制主机
    painter.drawPixmap(hero.x,hero.y,hero.heroPlane);

    //绘制子弹
    for(int i = 0;i < BULLET_NUM;i++){
        //若非空闲则绘制
        if(!hero.myBullet[i].isFree){
            painter.drawPixmap(hero.myBullet[i].my_x,hero.myBullet[i].my_y,hero.myBullet[i].myBullet);
        }
    }

    //绘制导弹
    for(int i = 0;i < BIG_BULLET_NUM;i++){
        //若非空闲则绘制
        if(!hero.bigBullet[i].isFree){
            painter.drawPixmap(hero.bigBullet[i].big_x,hero.bigBullet[i].big_y,hero.bigBullet[i].bigBullet);
        }
    }

    //绘制杂鱼
    for(int i = 0;i < FISH_NUM;i++){
        //若非空闲则绘制
        if(!fish[i].isFree){
           //若非空闲且未被摧毁则绘制杂鱼
            if(!fish[i].isDestoryed){
                if(i % 2)
                    painter.drawPixmap(fish[i].x,fish[i].y,fish[i].fishEnemy);
                else
                    painter.drawPixmap(fish[i].x,fish[i].y,fish[i].fishEnemy2);
            }
            //若被摧毁且当前爆炸图片未播放过则绘制爆炸图片
            else if(!fish[i].fishBomb.isPlayed){
                painter.drawPixmap(fish[i].fishBomb.x,fish[i].fishBomb.y,fish[i].fishBomb.bombPix[fish[i].fishBomb.index]);
            }
        }

        //绘制杂鱼子弹
        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!fish[i].fishBullet[j].isFree){
                painter.drawPixmap(fish[i].fishBullet[j].enemy_x,fish[i].fishBullet[j].enemy_y,fish[i].fishBullet[j].enemyBullet);
            }
        }
    }

    //绘制boss
    for(int i = 0;i < BOSS_NUM;i++){
        //若非空闲则绘制
        if(!boss[i].isFree){
           //若非空闲且未被摧毁则绘制boss
            if(!boss[i].isDestoryed){
                painter.drawPixmap(boss[i].x,boss[i].y,boss[i].boss);
            }
            //若被摧毁且当前爆炸图片未播放过则绘制爆炸图片
            else{
                for(int j = 0;j < BOSS_BOMB_NUM;j++){
                    if(!boss[i].bossBomb[j].isPlayed){
                        painter.drawPixmap(boss[i].bossBomb[j].x,boss[i].bossBomb[j].y,boss[i].bossBomb[j].bombPix[boss[i].bossBomb[j].index]);
                    }
                 }
            }
        }

        //绘制boss子弹
        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!boss[i].bossBullet1[j].isFree){
                painter.drawPixmap(boss[i].bossBullet1[j].boss_x,boss[i].bossBullet1[j].boss_y,boss[i].bossBullet1[j].bossBullet);
            }
        }

        for(int j = 0;j < BULLET_NUM;j++){
            //若非空闲则绘制
            if(!boss[i].bossBullet2[j].isFree){
                painter.drawPixmap(boss[i].bossBullet2[j].boss_x,boss[i].bossBullet2[j].boss_y,boss[i].bossBullet2[j].bossBullet);
            }
        }

        for(int j = 0;j < BULLET_NUM;j++){
             //若非空闲则绘制
             if(!boss[i].bossBullet3[j].isFree){
               painter.drawPixmap(boss[i].bossBullet3[j].boss_x,boss[i].bossBullet3[j].boss_y,boss[i].bossBullet3[j].bossBullet);
             }
        }
    }

    //绘制回血包
    for(int i = 0;i < LIFE_SUPPLY_NUM;i++){
        if(!lifeSupply[i].isFree){
            painter.drawPixmap(lifeSupply[i].supply_x,lifeSupply[i].supply_y,lifeSupply[i].lifeSupply);
        }
    }

    //生命值显示
    QFont font1("Courier",20);
    painter.setFont(font1);
    painter.setPen(Qt::white);
    painter.setBrush(Qt::white);
    painter.drawText(340,30,Life);
    painter.drawText(440,30,QString::number(hero.life));

    //得分显示
    QFont font2("Courier",20);
    painter.setFont(font2);
    painter.setPen(Qt::white);
    painter.setBrush(Qt::white);
    painter.drawText(340,100,Score);
    painter.drawText(440,100,QString::number(score));

    //技能值显示
    QFont font3("Courier",20);
    painter.setFont(font3);
    painter.setPen(Qt::white);
    painter.setBrush(Qt::white);
    painter.drawText(340,170,Skill);
    painter.drawText(440,170,QString::number(hero.skill));  
}

//杂鱼出场
void MainScene::showFish(){
    //累加时间记录
    fishRecorder++;

    //若未到达出场间隔则直接返回
    if(fishRecorder < FISH_SHOW_INTERVAL){
        return;
    }

    //否则重新置零
    fishRecorder = 0;

    //设置出场位置
    for(int i = 0;i < FISH_NUM;i++){
        //空闲且未被摧毁则出场
        if(fish[i].isFree && !fish[i].isDestoryed){
            fish[i].isFree = false;
            fish[i].x = rand()%(GAME_WIDTH - fish[i].fishRect.width());
            fish[i].y = -fish[i].fishRect.height();
            break;
        }
    }
}

//boss出场
void MainScene::showBoss(){
    //累加时间记录
    bossRecorder++;

    //若未到达出场间隔则直接返回
    if(bossRecorder < BOSS_SHOW_INTERVAL){
        return;
    }

    //否则重新置零
    bossRecorder = 0;

    //设置出场位置
    for(int i = 0;i < BOSS_NUM;i++){
        //空闲且未被摧毁则出场
        if(boss[i].isFree && !boss[i].isDestoryed){
            boss[i].isFree = false;
            boss[i].x = (GAME_WIDTH - boss[i].bossRect.width()) * 0.5;
            boss[i].y = -boss[i].bossRect.height();
            break;
        }
    }
}

//回血包掉落
void MainScene::showSupply(){
    //累加时间记录
    supplyRecorder++;

    //若未到达出场间隔则直接返回
    if(supplyRecorder < SUPPLY_SHOW_INTERVAL){
        return;
    }

    //否则重新置零
    supplyRecorder = 0;

    //设置出场位置
    for(int i = 0;i < LIFE_SUPPLY_NUM;i++){
        //空闲且未被摧毁则出场
        if(lifeSupply[i].isFree){
            lifeSupply[i].isFree = false;
            lifeSupply[i].supply_x = rand()%(GAME_WIDTH - lifeSupply[i].supplyRect.width());
            lifeSupply[i].supply_y = 0;
            break;
        }
    }
}

//碰撞检测
void MainScene::collisionDetection(){
    //遍历所有杂鱼
    for(int i = 0;i < FISH_NUM;i++){
        //若杂鱼非空闲且未被摧毁
        if(!fish[i].isFree && !fish[i].isDestoryed){
            //若主机与杂鱼相撞
            if(fish[i].fishRect.intersects(hero.myRect)){
                //主机生命值减二
                hero.life -= 2;

                //得分加十
                score += 10;

                if(hero.skill < HERO_SKILL){
                    //技能值加一
                    hero.skill++;
                }

                //改变杂鱼状态
                fish[i].isDestoryed = true;
                //fish[i].isFree = true;

                //设置爆炸坐标
                if(!fish[i].fishBomb.isPlayed){
                    fish[i].fishBomb.x = fish[i].x;
                    fish[i].fishBomb.y = fish[i].y;
                }
            }

            //遍历主机子弹
            for(int j = 0;j < BULLET_NUM;j++){
                //空闲子弹跳过
                if(hero.myBullet[j].isFree){
                    continue;
                }

                //若子弹击中敌机
                if(fish[i].fishRect.intersects(hero.myBullet[j].myRect)){
                    //改变子弹状态
                    hero.myBullet[j].isFree = true;

                    //得分加十
                    score += 10;

                    if(hero.skill < HERO_SKILL){
                        //技能值加一
                        hero.skill++;
                    }

                    //改变杂鱼状态
                    fish[i].isDestoryed = true;

                    //设置爆炸坐标
                    if(!fish[i].fishBomb.isPlayed){
                        fish[i].fishBomb.x = fish[i].x;
                        fish[i].fishBomb.y = fish[i].y;
                    }
                }
             }

            //遍历主机导弹
            for(int j = 0;j < BIG_BULLET_NUM;j++){
                //空闲子弹跳过
                if(hero.bigBullet[j].isFree){
                    continue;
                }

                //若子弹击中敌机
                if(fish[i].fishRect.intersects(hero.bigBullet[j].bigRect)){
                    //改变子弹状态
                    hero.bigBullet[j].isFree = true;

                    //得分加十
                    score += 10;

                    if(hero.skill < HERO_SKILL){
                        //技能值加一
                        hero.skill++;
                    }

                    //改变杂鱼状态
                    fish[i].isDestoryed = true;

                    //设置爆炸坐标
                    if(!fish[i].fishBomb.isPlayed){
                        fish[i].fishBomb.x = fish[i].x;
                        fish[i].fishBomb.y = fish[i].y;
                    }
                }
             }

            //遍历敌机子弹
            for(int j = 0;j < BULLET_NUM;j++){
                //空闲子弹跳过
                if(fish[i].fishBullet[j].isFree){
                    continue;
                }

                //若子弹击中主机
                if(hero.myRect.intersects(fish[i].fishBullet[j].enemyRect)){
                    //改变子弹状态
                    fish[i].fishBullet[j].isFree = true;

                    //主机生命值减一
                    hero.life--;
                }
            }
        }
    }

    //遍历boss
    for(int i = 0;i < BOSS_NUM;i++){
        //若boss空闲且未被摧毁
        if(!boss[i].isFree && !boss[i].isDestoryed){
            //若主机与boss相撞
            if(boss[i].bossRect.intersects(hero.myRect)){
                //主机生命值减五
                hero.life -= 5;

                //boss生命值减二
                boss[i].life -= 2;

                //若boss血量小于等于零
                if(boss[i].life <= 0){
                    //改变boss状态              
                    boss[i].isDestoryed = true;

                    //设置爆炸坐标
                    for(int j = 0;j < BOSS_BOMB_NUM;j++){
                        if(!boss[i].bossBomb[j].isPlayed){
                            boss[i].bossBomb[j].x = boss[i].x + j * 20;
                            boss[i].bossBomb[j].y = boss[i].y + 20;
                        }
                    }

                    //得分加二十
                    score += 20;

                    if(hero.skill < HERO_SKILL){
                        //技能值加三
                        hero.skill += 3;
                    }
                }
            }

            //遍历主机子弹
            for(int j = 0;j < BULLET_NUM;j++){
                //空闲子弹跳过
                if(hero.myBullet[j].isFree){
                    continue;
                }

                //若子弹击中boss
                if(boss[i].bossRect.intersects(hero.myBullet[j].myRect)){
                    //改变子弹状态
                    hero.myBullet[j].isFree = true;

                    //boss生命值减一
                    boss[i].life--;

                    //若boss血量小于等于零
                    if(boss[i].life <= 0){                    
                        boss[i].isDestoryed = true;

                        //设置爆炸坐标
                        for(int j = 0;j < BOSS_BOMB_NUM;j++){
                            if(!boss[i].bossBomb[j].isPlayed){
                                boss[i].bossBomb[j].x = boss[i].x + j * 45;
                                boss[i].bossBomb[j].y = boss[i].y + 20;
                            }
                        }
                        //得分加二十
                        score += 20;

                        if(hero.skill < HERO_SKILL){
                            //技能值加三
                            hero.skill += 3;
                        }
                    }
                }
            }

            //遍历主机导弹
            for(int j = 0;j < BIG_BULLET_NUM;j++){
                //空闲子弹跳过
                if(hero.bigBullet[j].isFree){
                    continue;
                }

                //若子弹击中boss
                if(boss[i].bossRect.intersects(hero.bigBullet[j].bigRect)){
                    //改变子弹状态
                    hero.bigBullet[j].isFree = true;

                    //boss生命值减五
                    boss[i].life -= 5;

                    //若boss血量小于等于零
                    if(boss[i].life <= 0){
                        //改变boss状态
                        boss[i].isDestoryed = true;

                        //设置爆炸坐标
                        for(int j = 0;j < BOSS_BOMB_NUM;j++){
                            if(!boss[i].bossBomb[j].isPlayed){
                                boss[i].bossBomb[j].x = boss[i].x + j * 45;
                                boss[i].bossBomb[j].y = boss[i].y + 20;
                            }
                        }
                        //得分加二十
                        score += 20;

                        if(hero.skill < HERO_SKILL){
                            //技能值加三
                            hero.skill += 3;
                        }
                    }
                }
            }

            //遍历boss子弹
            for(int j = 0;j < BULLET_NUM;j++){ //遍历第一个弹匣
                if(boss[i].bossBullet1[j].isFree){
                    continue;
                }

                if(hero.myRect.intersects(boss[i].bossBullet1[j].bossRect)){
                    hero.life--;
                    boss[i].bossBullet1[j].isFree = true;
                }
            }

            for(int j = 0;j < BULLET_NUM;j++){ //遍历第二个弹匣
                if(boss[i].bossBullet2[j].isFree){
                    continue;
                }

                if(hero.myRect.intersects(boss[i].bossBullet2[j].bossRect)){
                    hero.life--;
                    boss[i].bossBullet2[j].isFree = true;
                }
            }

            for(int j = 0;j < BULLET_NUM;j++){ //遍历第三个弹匣
                if(boss[i].bossBullet3[j].isFree){
                    continue;
                }

                if(hero.myRect.intersects(boss[i].bossBullet3[j].bossRect)){
                    hero.life--;
                    boss[i].bossBullet3[j].isFree = true;
                }
            }
        }
    }

    //遍历回血包
    for(int i = 0;i < LIFE_SUPPLY_NUM;i++){
        //空闲回血包跳过
        if(lifeSupply[i].isFree){
            continue;
        }

        //若回血包击中主机
        if(hero.myRect.intersects(lifeSupply[i].supplyRect)){
            //改变回血包状态
            lifeSupply[i].isFree = true;

            //若主机血量未满，主机生命值加一
            if(hero.life < HERO_LIFE){
                hero.life++;
            }
        }
    }
}

//移动鼠标事件
void MainScene::mouseMoveEvent(QMouseEvent *event){
    int x = event->x() - hero.myRect.width() * 0.5;
    int y = event->y() - hero.myRect.height() * 0.5;

    //改变飞机坐标，注意边界检测
    if(x <= 0){
        x = 0;
    }
    if(x >= GAME_WIDTH- hero.myRect.width()){
        x = GAME_WIDTH- hero.myRect.width();
    }
    if(y <= 0){
        y = 0;
    }
    if(y >= GAME_HEIGHT- hero.myRect.height()){
        y = GAME_HEIGHT- hero.myRect.height();
    }

    hero.setPosition(x,y);
}

//键盘事件
void MainScene::keyPressEvent(QKeyEvent *event){
    //Q技能发射导弹
    if(event->key() == Qt::Key_Q){
        //扣三点技能值
        if(hero.skill >= 3){
            for(int i = 0;i < BIG_BULLET_NUM;i++){
                //选择空闲导弹进行发射
                if(hero.bigBullet[i].isFree){
                    //设置发射位置
                    hero.skill -= 3;
                    hero.bigBullet[i].big_x = hero.x + hero.myRect.width() * 0.5 - 15;
                    hero.bigBullet[i].big_y = hero.y - 25;
                    //改变导弹状态
                    hero.bigBullet[i].isFree = false;
                    break;
                }
            }
        }
    }

    //W技能回血
    else if(event->key() == Qt::Key_W){
       if(hero.life == HERO_LIFE){
            return;
        }

       //扣两点技能值
        if(hero.skill >= 2){
            hero.skill -= 2;
            //加一点血
            hero.life++;
        }
    }

    //E技能增加攻速
    else if(event->key() == Qt::Key_E){
        //若攻速已被改变过，则恢复
        if(hero.isChanged){
            hero.isChanged = false;
            hero.interval = INTERVAL;
        }
        //否则改变
        else{
            hero.isChanged = true;
            //扣一点技能值
            if(hero.skill >= 1){
                //加攻速
                hero.interval /= 2;
            }
        }
    }

    //R技能清屏
    else if(event->key() == Qt::Key_R){
        //扣十点技能值
        if(hero.skill >= 10){
            for(int i = 0;i < FISH_NUM;i++){
                if(!fish[i].isFree && !fish[i].isDestoryed){
                    score += 10;
                    fish[i].isDestoryed = true;
                    //设置爆炸坐标
                    if(!fish[i].fishBomb.isPlayed){
                        fish[i].fishBomb.x = fish[i].x;
                        fish[i].fishBomb.y = fish[i].y;
                    }
                }
            }

            for(int i = 0;i < BOSS_NUM;i++){
                if(!boss[i].isFree && !boss[i].isDestoryed){
                    score += 20;
                    boss[i].isDestoryed = true;
                    //设置爆炸坐标
                    for(int j = 0;j < BOSS_BOMB_NUM;j++){
                        if(!boss[i].bossBomb[j].isPlayed){
                            boss[i].bossBomb[j].x = boss[i].x + j * 45;
                            boss[i].bossBomb[j].y = boss[i].y + 20;
                        }
                    }
                }
            }

            hero.skill -= 10;
        }
    }

    update();
}

//游戏结束函数
void MainScene::game_over(){
    ui->score = this->score;
    if(hero.life == 0){
        timer.stop();
        ui->show();
    }
}

MainScene::~MainScene()
{
}

