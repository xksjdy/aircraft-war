#include "heroplane.h"
#include "config.h"

HeroPlane::HeroPlane()
{
    //初始化加载主机图片对象
    heroPlane.load(HEROPLANE_PATH);

    //初始化主机坐标
    x = (GAME_WIDTH - heroPlane.width()) * 0.5;
    y = GAME_HEIGHT - heroPlane.height();

    //初始化矩形边框
    myRect.setWidth(heroPlane.width());
    myRect.setHeight(heroPlane.height());
    myRect.moveTo(x,y);

    //设置生命值
    life = HERO_LIFE;

    //设置技能值
    skill = HERO_SKILL;

    //初始化射击时间记录
    recorder = 0;
    interval = INTERVAL;

    //攻速无改变
    isChanged = false;
}

//射击函数
void HeroPlane::shoot(){
    //累加时间记录
    recorder++;

    //若当前记录时间小于射击间隔，则返回
    if(recorder < interval){
        return;
    }

    //若达到射击间隔，则将时间记录重新置零
    recorder = 0;

    if(interval < INTERVAL && skill > 0){
        skill -= 0.1;
    }

    if(skill == 0){
        interval = INTERVAL;
    }

    //发射子弹
    for(int i = 0;i < BULLET_NUM;i++){
        //选择空闲子弹进行发射
        if(myBullet[i].isFree){
            //设置发射位置
            myBullet[i].my_x = x + myRect.width() * 0.5 - 15;
            myBullet[i].my_y = y - 25;
            //发射后更改子弹空闲状态
            myBullet[i].isFree = false;
            break;
        }
    }
}

//位置函数
void HeroPlane::setPosition(int x, int y){
    this->x = x;
    this->y = y;
    myRect.moveTo(x,y);
}
