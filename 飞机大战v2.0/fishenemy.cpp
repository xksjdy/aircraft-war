#include "fishenemy.h"
#include "config.h"

FishEnemy::FishEnemy()
{
    //初始化加载杂鱼图片对象
    fishEnemy.load(FISHENEMY_PATH);
    fishEnemy2.load(FISHENEMY2_PATH);

    //设置杂鱼坐标
    x = 0;
    y = 0;

    //设置杂鱼生命值
    life = 3;

    //初始化矩形边框
    fishRect.setWidth(fishEnemy.width());
    fishRect.setHeight(fishEnemy.height());
    fishRect.moveTo(x,y);

    //不在界面内（空闲）
    isFree = true;

    //尚未被摧毁
    isDestoryed = false;

    //设置速度
    speed = FISH_SPEED;

    //初始化射击时间记录
    recorder = 0;
}

//位置更新函数
void FishEnemy::updatePosition(){
    //若不在界面内（空闲），则直接返回
    if(isFree){
        return;
    }

    //否则向下运动
    y += FISH_SPEED;
    fishRect.moveTo(x,y);

    //超出界面则重新置为空闲
    if(y >GAME_HEIGHT + fishRect.height()){
        isFree = true;
    }
}

//射击函数
void FishEnemy::shoot(){
    //累加时间记录
    recorder++;

    //若当前记录时间小于射击间隔，则返回
    if(recorder < FISH_INTERVAL){
        return;
    }

    //若达到射击间隔，则将时间记录重新置零
    recorder = 0;

    //发射子弹
    for(int i = 0;i < BULLET_NUM;i++){
        //选择空闲子弹进行发射
        if(fishBullet[i].isFree){
            //设置发射位置
            fishBullet[i].enemy_x = x + fishRect.width() * 0.5 - 10;
            fishBullet[i].enemy_y = y + 25;
            //发射后更改子弹空闲状态
            fishBullet[i].isFree = false;
            break;
        }
    }
}

