#include "bullet.h"
#include "config.h"

Bullet::Bullet()
{
    //初始化加载子弹对象
    myBullet.load(MY_BULLET_PATH);
    bigBullet.load(BIG_BULLET_PATH);
    enemyBullet.load(ENEMY_BULLET_PATH);
    bossBullet.load(BOSS_BULLET_PATH);
    lifeSupply.load(LIFE_SUPPLY);

    //设置子弹速度
    x_speed = X_BULLET_SPEED;
    y_speed = Y_BULLET_SPEED;

    //设置子弹坐标
    my_x = (GAME_WIDTH - myBullet.width()) * 0.5;
    my_y = GAME_HEIGHT;
    big_x = (GAME_WIDTH - myBullet.width()) * 0.5;
    big_y = GAME_HEIGHT;
    enemy_x = (GAME_WIDTH - enemyBullet.width()) * 0.5;
    enemy_y = 0;
    boss_x = (GAME_WIDTH - bossBullet.width()) * 0.5;
    boss_y = 0;
    supply_x = (GAME_WIDTH - lifeSupply.width()) * 0.5;
    supply_y = 0;

    //设置子弹边框
    myRect.setWidth(myBullet.width());
    myRect.setHeight(myBullet.height());
    myRect.moveTo(my_x,my_y);

    bigRect.setWidth(bigBullet.width());
    bigRect.setHeight(bigBullet.height());
    bigRect.moveTo(big_x,big_y);

    enemyRect.setWidth(enemyBullet.width());
    enemyRect.setHeight(enemyBullet.height());
    enemyRect.moveTo(enemy_x,enemy_y);

    bossRect.setWidth(bossBullet.width());
    bossRect.setHeight(bossBullet.height());
    bossRect.moveTo(boss_x,boss_y);

    supplyRect.setWidth(lifeSupply.width());
    supplyRect.setHeight(lifeSupply.height());
    supplyRect.moveTo(supply_x,supply_y);

    //设置子弹状态
    isFree = true;
}

//我方子弹位置更新函数
void Bullet::updatePosition(){
    //若子弹空闲，不计算坐标，直接返回，否则向上运动
    if(isFree){
        return;
    }
    my_y -= y_speed;
    myRect.moveTo(my_x,my_y);

    //若子弹超出界面，则设置为空闲状态
    if(my_y <= -myRect.height()){
        isFree = true;
    }
}

//导弹位置更新函数
void Bullet::updateBigPosition(){
    //若导弹空闲，不计算坐标，直接返回，否则向上运动
    if(isFree){
        return;
    }
    big_y -= y_speed;
    bigRect.moveTo(big_x,big_y);

    //若导弹超出界面，则设置为空闲状态
    if(big_y <= -bigRect.height()){
        isFree = true;
    }
}

//杂鱼子弹位置更新函数
void Bullet::updateEnemyPosition(){
    if(isFree){
        return;
    }
    enemy_y += FISH_SPEED + y_speed;
    enemyRect.moveTo(enemy_x,enemy_y);

    //若子弹超出界面，则设置为空闲状态
    if(enemy_y >= GAME_HEIGHT + enemyRect.height()){
        isFree = true;
    }
}

//boss子弹位置更新函数
//子弹竖直向下运动
void Bullet::updateBossPosition(){
    if(isFree){
        return;
    }
    boss_y += BOSS_SPEED + y_speed;
    bossRect.moveTo(boss_x,boss_y);

    //若子弹超出界面，则设置为空闲状态
    if(boss_y >= GAME_HEIGHT + bossRect.height()){
        isFree = true;
    }
}

//子弹偏左运动
void Bullet::updateBossPositionLeft(){
    if(isFree){
        return;
    }
    boss_x -= x_speed;
    boss_y += BOSS_SPEED + y_speed;
    bossRect.moveTo(boss_x,boss_y);

    //若子弹超出界面，则设置为空闲状态
    if(boss_y >= GAME_HEIGHT + bossRect.height()){
        isFree = true;
    }
    //若子弹超出界面，则设置为空闲状态
    if(boss_x <= -bossRect.width()){
        isFree = true;
    }
}

//子弹偏右运动
void Bullet::updateBossPositionRight(){
    if(isFree){
        return;
    }
    boss_x += x_speed;
    boss_y += BOSS_SPEED + y_speed;
    bossRect.moveTo(boss_x,boss_y);

    //若子弹超出界面，则设置为空闲状态
    if(boss_y >= GAME_HEIGHT + bossRect.height()){
        isFree = true;
    }

    //若子弹超出界面，则设置为空闲状态
    if(boss_x >= GAME_WIDTH + bossRect.width()){
        isFree = true;
    }
}

//回血包掉落
void Bullet::updateSupplyPosition(){
    if(isFree){
        return;
    }
    supply_y += SUPPLY_SPEED;
    supplyRect.moveTo(supply_x,supply_y);

    //若回血包超出界面，则设置为空闲状态
    if(supply_y >= GAME_HEIGHT + supplyRect.height()){
        isFree = true;
    }
}
