#ifndef MAINSCENE_H
#define MAINSCENE_H


#include <QWidget>
#include <QTimer>
#include <QLabel>
#include "map.h"
#include "heroplane.h"
#include "fishenemy.h"
#include "boss.h"
#include "bomb.h"
#include "gameover.h"


class MainScene : public QWidget
{
    Q_OBJECT

public:
    MainScene(QWidget *parent = nullptr);
    ~MainScene();

    //生命条显示
    QString Life;

    //得分显示
    QString Score;

    //技能值显示
    QString Skill;

    //得分
    int score;

    //杂鱼出场间隔
    int fishRecorder;

    //boss出场间隔
    int bossRecorder;

    //记录杂鱼出场数量
    int fish_num;

    //回血包时间间隔
    int supplyRecorder;

    //初始化窗口大小
    void initScene();

    //启动游戏
    void playGame();

    //更新游戏中所有元素坐标
    void updatePosition();

    //绘制屏幕事件
    void paintEvent(QPaintEvent *event);

    //移动鼠标事件
    void mouseMoveEvent(QMouseEvent *event);

    //Boss出场
    void showBoss();

    //杂鱼出场
    void showFish();

    //回血包掉落
    void showSupply();

    //碰撞检测
    void collisionDetection();

    //键盘事件
    void keyPressEvent(QKeyEvent *event);

    //游戏结束函数
    void game_over();

    //地图对象
    Map map;

    //主机对象
    HeroPlane hero;

    //杂鱼对象数组
    FishEnemy fish[FISH_NUM];

    //Boss对象数组
    Boss boss[BOSS_NUM];

    //回血包对象数组
    Bullet lifeSupply[LIFE_SUPPLY_NUM];

    //定时器对象
    QTimer timer;

    //游戏结束对象
    GameOver *ui;
};
#endif // MAINSCENE_H
