#include "map.h"
#include "config.h"

Map::Map()
{
    //初始化加载地图对象
     map1.load(MAP_PATH);
     map2.load(MAP_PATH);

     //设置地图y轴坐标
     map1_Y = -GAME_HEIGHT;
     map2_Y = 0;

     //设置地图滚动速度
     scroll_speed = MAP_SCROLL_SPEED;
}

void Map::mapPosition(){
    //处理第一张图片滚动
    map1_Y += scroll_speed;
    if(map1_Y >= 0){
        map1_Y = -GAME_HEIGHT;
    }

    //处理第二张图片滚动
    map2_Y += scroll_speed;
    if(map2_Y >= GAME_HEIGHT){
        map2_Y = 0;
    }
}
