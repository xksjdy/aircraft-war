#ifndef BULLET_H
#define BULLET_H

#include <QPixmap>
#include <QString>
#include <QRect>

class Bullet
{
public:
    //构造函数
    Bullet();

    //我方子弹图片
    QPixmap myBullet;
    QPixmap bigBullet;

    //敌方子弹图片
    QPixmap enemyBullet;

    //Boss子弹图片
    QPixmap bossBullet;

    //回血包图片
    QPixmap lifeSupply;

    //子弹速度
    int x_speed;
    int y_speed;

    //子弹坐标
    int my_x;
    int my_y;
    int big_x;
    int big_y;
    int enemy_x;
    int enemy_y;
    int boss_x;
    int boss_y;
    int supply_x;
    int supply_y;

    //矩形边框（用于碰撞检测）
    QRect myRect;
    QRect enemyRect;
    QRect bossRect;
    QRect supplyRect;
    QRect bigRect;

    //更新我方子弹位置
    void updatePosition();
    void updateBigPosition();

    //更新敌方子弹位置
    void updateEnemyPosition();
    void updateBossPosition();
    void updateBossPositionLeft();
    void updateBossPositionRight();

    //更新回血包位置
    void updateSupplyPosition();

    //子弹是否在界面之内（是否空闲）
    bool isFree;
};

#endif // BULLET_H
