#ifndef HEROPLANE_H
#define HEROPLANE_H

#include <QPoint>
#include <QPixmap>
#include <QRect>
#include "bullet.h"
#include "config.h"

class HeroPlane
{
public:
    //构造函数
    HeroPlane();

    //弹匣
    Bullet myBullet[BULLET_NUM];

    //我方飞机图片对象
    QPixmap heroPlane;

    //我方飞机坐标
    int x;
    int y;

    //矩形边框
    QRect myRect;

    //飞机射击时间记录
    int recorder;

    //生命值
    int life;

    //技能值
    double skill;

    //射击函数
    void shoot();

    //位置函数
    void setPosition(int,int);
};

#endif // HEROPLANE_H
