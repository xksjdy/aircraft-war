#include "gameover.h"
#include "ui_gameover.h"
#include <QPainter>


GameOver::GameOver(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameOver)
{
    ui->setupUi(this);
}

void GameOver::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    QFont font("Courier",20);
    painter.setFont(font);
    painter.setPen(Qt::black);
    painter.setBrush(Qt::black);
    painter.drawText(180,195,QString::number(score));
}

GameOver::~GameOver()
{
    delete ui;
}
